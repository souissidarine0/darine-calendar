import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { range } from 'ramda';

@Component({
  selector: 'datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  host: {
    '(document:click)': 'onCloseCalendar($event)',
  },
})
export class DatepickerComponent implements OnInit {

  @Input() value: string;
  @Output() update: EventEmitter<string> = new EventEmitter<string>();
  date: Date = new Date();
  month: number;
  year: number;
  days: number[];

  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  showCalendar: boolean = false;
  result: string;

  ngOnInit() {

    if (this.value) this.date = new Date(this.value);
    this.month = this.date.getMonth();
    console.log("month", this.month)
    this.year = this.date.getFullYear();
    if (this.value) this.selectDay(this.date.getDate());
    this.updateMonth();
  }

/**
 * Updates year, month and day on every event change
 * @param e : event 
 * @param type : event type (increment , decrement)
 */
  updateMonth(e?: Event, type?: string) {
    if (e) e.stopPropagation();
    if (type === 'dec') this.month--;
    if (type === 'inc') this.month++;
    if (this.month < 0) {
      this.month = 11;
      this.year--;
    }
    if (this.month > 11) {
      this.month = 0;
      this.year++;
    }
    const date = new Date(this.year, this.month+1, 0);
    const date2 = new Date(this.year, this.month, 0);

    const days = date.getDate()
    console.log("days",days)

    const day = date2.getDay();
    console.log("day",day)

    const prefix = new Array(day);
    this.days = prefix.concat(range(1, days+1));
    console.log("days",this.days)

  }

  /**
   * Detect selected day and updates the output (update)
   * @param day : detected day
   */
  selectDay(day: number) {
    if (!day) return;
    const pad = (s) => (s.length < 2 ? 0 + s : s);
    this.date = new Date(this.year, this.month, day);
    this.result = `${this.year}-${pad(this.month + 1 + '')}-${pad(day + '')}`;
    this.update.emit(this.result);

  }
/**
 * Shows the calendar on event click
 * @param e : event click
 */
  onShowCalendar(e: Event) {
    e.stopPropagation();
    this.showCalendar = true;
  }

  /**
   * Hide the calendar on event click 
   * @param e : event click
   */
  onCloseCalendar(e: Event) {
    if (this.showCalendar) {
      this.showCalendar = false;
    }
    return;
  }


}
