import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DateModule } from './DateModule/dateModule.module';
import { AppComponent } from './app.component';

@NgModule({
  imports: [BrowserModule, DateModule],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
})
export class AppModule { }
